package forward

import (
	"fmt"
	"io"
	"log"
	"net"
	"time"

	"gitlab.com/RandomCivil/common/bytespool"
)

type TcpForwarder struct {
	ErrCh    chan error
	Timeout  int
	Listener net.Listener
	Dst      *Dst
	Port     int
}

func NewTcpForwarder() *TcpForwarder {
	return &TcpForwarder{
		ErrCh:   make(chan error),
		Timeout: 10,
	}
}

func (f *TcpForwarder) Listen() error {
	log.Println(fmt.Sprintf(":%d", f.Port))
	l, err := net.Listen("tcp", fmt.Sprintf(":%d", f.Port))
	if err != nil {
		return err
	}
	f.Listener = l
	return nil
}

func (f *TcpForwarder) Accept() {
	conn, err := f.Listener.Accept()
	if err != nil {
		f.ErrCh <- err
		return
	}
	go f.handleConn(conn)
}

func (f *TcpForwarder) Close() error {
	return f.Listener.Close()
}

func (f *TcpForwarder) handleConn(conn net.Conn) {
	defer conn.Close()

	dialer := net.Dialer{Timeout: time.Second * time.Duration(f.Timeout)}
	target, err := dialer.Dial(f.Dst.Network, f.Dst.Addr)
	if err != nil {
		f.ErrCh <- err
		return
	}
	defer target.Close()

	errCh := make(chan error, 2)
	go f.pipe(target, conn, errCh)
	go f.pipe(conn, target, errCh)
	<-errCh
}

func (f *TcpForwarder) pipe(dst io.Writer, src io.Reader, errCh chan error) int64 {
	buf := bytespool.Alloc(int32(largeBufferSize))
	defer bytespool.Free(buf)

	n, err := io.CopyBuffer(dst, src, buf)
	log.Printf("pipe n:%d,err:%v\n", n, err)
	errCh <- err
	return n
}

func (f *TcpForwarder) SetPort(port int) {
	f.Port = port
}

func (f *TcpForwarder) SetDst(d *Dst) {
	f.Dst = d
}

func (f *TcpForwarder) ErrChan() chan error {
	return f.ErrCh
}

func (f *TcpForwarder) Clean(threshold int64, interval int) {
}

func (f *TcpForwarder) SendConnNum() int {
	return 0
}
