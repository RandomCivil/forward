package forward

import (
	"log"
	"net"
	"reflect"
	"strconv"
	"sync"
	"testing"
	"time"

	"gitlab.com/RandomCivil/common/bytespool"
)

type mockUdpServer struct {
	conn   *net.UDPConn
	stopCh chan struct{}
	doneCh chan struct{}
	readCh chan string
	once   sync.Once
}

func newMockUdpServer() *mockUdpServer {
	return &mockUdpServer{
		stopCh: make(chan struct{}, 1),
		doneCh: make(chan struct{}),
		readCh: make(chan string, 100),
		once:   sync.Once{},
	}
}

func (s *mockUdpServer) start() {
	addr := &net.UDPAddr{Port: 0}
	ln, err := net.ListenUDP("udp", addr)
	if err != nil {
		panic(err)
	}
	log.Println("mockUdpServer listen on", ln.LocalAddr().String())

	s.conn = ln
	s.doneCh <- struct{}{}
	s.handle()
}

func (s *mockUdpServer) stop() {
	s.once.Do(func() { s.conn.Close() })
}

func (s *mockUdpServer) handle() {
	buf := bytespool.Alloc(100)
	defer bytespool.Free(buf)

	for {
		n, addr, err := s.conn.ReadFromUDP(buf)
		if err != nil {
			log.Printf("ReadFromUDP error %v", err)
			close(s.readCh)
			return
		}
		recv := string(buf[:n])
		log.Println(recv)
		s.readCh <- recv
		s.conn.WriteToUDP([]byte("reply "+recv), addr)
	}
}

func TestUdp(t *testing.T) {
	s := newMockUdpServer()
	go s.start()
	defer s.stop()
	<-s.doneCh

	f, err := NewForwarder(2000, "udp://"+s.conn.LocalAddr().String(), 10)
	if err != nil {
		panic(err)
	}
	go f.Start()
	<-f.doneCh

	data := make(map[string]struct{})
	for i := 1; i <= 10; i++ {
		data["hello world "+strconv.Itoa(i)] = struct{}{}
	}

	var (
		crecv, srecv []string
		mu           sync.Mutex
	)
	wg := sync.WaitGroup{}
	wg.Add(10)

	for d, _ := range data {
		buf := bytespool.Alloc(1000)
		go func(d string) {
			dstAddr, err := net.ResolveUDPAddr("udp", ":2000")
			conn, err := net.DialUDP("udp", nil, dstAddr)
			if err != nil {
				panic(err)
			}
			conn.Write([]byte(d))

			go func() {
				n, _, err := conn.ReadFromUDP(buf)
				if err != nil {
					log.Printf("ReadFromUDP error %v", err)
					return
				}
				recv := string(buf[:n])
				t.Log(recv)
				mu.Lock()
				crecv = append(crecv, recv)
				mu.Unlock()
			}()

			time.Sleep(100 * time.Millisecond)
			bytespool.Free(buf)
			conn.Close()
			wg.Done()
		}(d)
	}
	wg.Wait()
	go s.stop()

	mu.Lock()
	if !checkServerRecv(data, crecv, "reply ") {
		t.Fatalf("TestUdp crecv err,output:%v,expected:%v\n", crecv, data)
	}

	for recv := range s.readCh {
		srecv = append(srecv, recv)
	}

	if !checkServerRecv(data, srecv, "") {
		t.Fatalf("TestUdp srecv err,output:%v,expected:%v\n", srecv, data)
	}
	mu.Unlock()
	f.stopCh <- struct{}{}
}

func TestClean(t *testing.T) {
	s := newMockUdpServer()
	go s.start()
	defer s.stop()
	<-s.doneCh

	f, err := NewForwarder(2001, "udp://"+s.conn.LocalAddr().String(), 10)
	if err != nil {
		panic(err)
	}
	go f.Start()
	<-f.doneCh

	data := make(map[string]struct{})
	for i := 1; i <= 10; i++ {
		data["hello world "+strconv.Itoa(i)] = struct{}{}
	}

	go f.Instance.Clean(3, 1)

	var (
		conns []*net.UDPConn
		mu    sync.Mutex
	)
	wg := sync.WaitGroup{}
	wg.Add(10)

	for d, _ := range data {
		buf := bytespool.Alloc(1000)
		go func(d string) {
			dstAddr, err := net.ResolveUDPAddr("udp", ":2001")
			conn, err := net.DialUDP("udp", nil, dstAddr)
			if err != nil {
				panic(err)
			}
			mu.Lock()
			conns = append(conns, conn)
			mu.Unlock()
			conn.Write([]byte(d))

			time.Sleep(100 * time.Millisecond)
			bytespool.Free(buf)
			wg.Done()
		}(d)
	}
	wg.Wait()

	go func() {
		time.Sleep(2 * time.Second)
		for i := 0; i < 5; i++ {
			conns[i].Write([]byte("resend " + strconv.Itoa(i)))
		}
	}()

	var (
		nums []int
		nn   int
		wait = make(chan struct{}, 1)
	)

	go func() {
		ticker := time.NewTicker(2 * time.Second)
		defer ticker.Stop()

		for {
			select {
			case <-ticker.C:
				if nn == 3 {
					wait <- struct{}{}
					return
				}
				n := f.Instance.SendConnNum()
				t.Log("nums of send map", n)
				nums = append(nums, n)
				nn++
			}
		}
	}()

	<-wait

	for _, conn := range conns {
		conn.Close()
	}
	f.stopCh <- struct{}{}

	t.Log("nums", nums)
	if !reflect.DeepEqual(nums, []int{10, 5, 0}) {
		t.Fatalf("TestClean error,nums:%v\n", nums)
	}
}
