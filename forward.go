package forward

import (
	"log"
)

const (
	largeBufferSize = 32 * 1024
)

func init() {
	log.SetFlags(log.Ldate | log.Lmicroseconds | log.Lshortfile)
}

type NetworkForwarder interface {
	Listen() error
	Accept()
	Close() error
	SetPort(port int)
	SetDst(d *Dst)
	ErrChan() chan error
	Clean(threshold int64, interval int)
	SendConnNum() int
}

type Forwarder struct {
	Port     int
	Dst      string
	Timeout  int
	ErrCh    chan error
	stopCh   chan struct{}
	doneCh   chan struct{}
	Instance NetworkForwarder
}

func NewForwarder(port int, dst string, timeout int) (*Forwarder, error) {
	d := NewDst(dst)
	err := d.parse()
	log.Printf("dst:%+v %v\n", d, err)
	if err != nil {
		return nil, err
	}

	var instance NetworkForwarder
	if d.Network == "udp" {
		instance = NewUdpForwarder()
	} else {
		instance = NewTcpForwarder()
	}
	instance.SetDst(d)
	instance.SetPort(port)

	return &Forwarder{
		Port:     port,
		Dst:      dst,
		Timeout:  timeout,
		ErrCh:    make(chan error),
		stopCh:   make(chan struct{}, 1),
		doneCh:   make(chan struct{}, 1),
		Instance: instance,
	}, nil
}

func (f *Forwarder) Start() {
	err := f.Instance.Listen()
	if err != nil {
		f.ErrCh <- err
		return
	}
	f.doneCh <- struct{}{}

	for {
		select {
		case err = <-f.Instance.ErrChan():
			f.ErrCh <- err
			return
		case <-f.stopCh:
			f.Instance.Close()
			return
		default:
			f.Instance.Accept()
		}
	}
}
