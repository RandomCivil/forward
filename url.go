package forward

import (
	"errors"
	"net/url"
)

type Dst struct {
	dst, Network, Addr string
}

func NewDst(dst string) *Dst {
	return &Dst{dst: dst}
}

func (d *Dst) parse() error {
	u, err := url.Parse(d.dst)
	if err != nil {
		return err
	}
	switch u.Scheme {
	case "tcp":
		d.Network = "tcp"
	case "tcp6":
		d.Network = "tcp6"
	case "udp":
		d.Network = "udp"
	default:
		return errors.New("network is not tcp and udp")
	}
	d.Addr = u.Host
	return nil
}
