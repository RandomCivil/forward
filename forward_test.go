package forward

import "testing"

func TestErrDst(t *testing.T) {
	_, err := NewForwarder(2000, "abc://10.0.0.1:3000", 5)
	if err == nil {
		t.Fatalf("TestErrDst error\n")
	}
}
