package forward

import (
	"fmt"
	"io"
	"log"
	"net"
	"strconv"
	"sync"
	"testing"
	"time"

	"gitlab.com/RandomCivil/common/bytespool"
)

type mockTcpServer struct {
	ln     net.Listener
	stopCh chan struct{}
	doneCh chan struct{}
	readCh chan string
}

func newMockTcpServer() *mockTcpServer {
	return &mockTcpServer{
		stopCh: make(chan struct{}, 1),
		doneCh: make(chan struct{}),
		readCh: make(chan string, 10),
	}
}

func (s *mockTcpServer) start() {
	ln, err := net.Listen("tcp", "")
	if err != nil {
		panic(err)
	}
	log.Println("mockTcpServer listen on", ln.Addr().String())

	s.ln = ln
	s.doneCh <- struct{}{}
	for {
		conn, err := ln.Accept()
		if err != nil {
			log.Printf("mockTcpServer accept error %v\n", err)
			return
		}
		go s.handle(conn)
	}
}

func (s *mockTcpServer) stop() {
	s.ln.Close()
}

func (s *mockTcpServer) handle(conn net.Conn) {
	buf := bytespool.Alloc(100)
	defer bytespool.Free(buf)

	for {
		n, err := conn.Read(buf)
		if err != nil {
			if err == io.EOF {
				return
			}
			panic(err)
		}
		recv := string(buf[:n])
		log.Println(recv)
		s.readCh <- recv
		conn.Write([]byte("reply " + recv))
	}
}

func TestTcp(t *testing.T) {
	s := newMockTcpServer()
	go s.start()
	defer s.stop()
	<-s.doneCh

	f, err := NewForwarder(2000, "tcp://"+s.ln.Addr().String(), 5)
	if err != nil {
		panic(err)
	}
	go f.Start()
	<-f.doneCh

	data := make(map[string]struct{})
	for i := 1; i <= 10; i++ {
		data["hello world "+strconv.Itoa(i)] = struct{}{}
	}

	var (
		crecv, srecv []string
		mu           sync.Mutex
	)
	wg := sync.WaitGroup{}
	wg.Add(10)
	for d, _ := range data {
		buf := bytespool.Alloc(100)

		go func(d string) {
			conn, err := net.Dial("tcp", "127.0.0.1:2000")
			if err != nil {
				panic(err)
			}
			fmt.Fprintf(conn, d)

			go func() {
				for {
					n, err := conn.Read(buf)
					if err != nil {
						return
					}
					recv := string(buf[:n])
					t.Log("client recv", recv)
					mu.Lock()
					crecv = append(crecv, recv)
					mu.Unlock()
				}
			}()

			time.Sleep(100 * time.Millisecond)
			conn.Close()
			bytespool.Free(buf)
			wg.Done()
		}(d)
	}
	wg.Wait()
	close(s.readCh)

	if !checkServerRecv(data, crecv, "reply ") {
		t.Fatalf("TestTcp crecv err,output:%v,expected:%v\n", crecv, data)
	}

	for recv := range s.readCh {
		srecv = append(srecv, recv)
	}

	if !checkServerRecv(data, srecv, "") {
		t.Fatalf("TestTcp srecv err,output:%v,expected:%v\n", srecv, data)
	}
	f.stopCh <- struct{}{}
}

func checkServerRecv(data map[string]struct{}, recv []string, prefix string) bool {
	size := len(prefix)
	if len(data) != len(recv) {
		return false
	}
	for _, r := range recv {
		if size > 0 {
			if r[:size] != prefix {
				return false
			}
			r = r[size:]
		}
		if _, ok := data[r]; !ok {
			return false
		}
	}
	return true
}
