package forward

import (
	"net"
	"runtime"
	"sync"
	"time"

	"log"

	"gitlab.com/RandomCivil/common/bytespool"
)

type sendConn struct {
	conn   *net.UDPConn
	ts     time.Time
	src    *net.UDPAddr
	stopCh chan struct{}
	readCh chan int
	mu     sync.RWMutex
}

func newSendConn(src *net.UDPAddr, sconn *net.UDPConn) *sendConn {
	return &sendConn{
		conn:   sconn,
		ts:     time.Now(),
		src:    src,
		stopCh: make(chan struct{}, 1),
		readCh: make(chan int, 10),
		mu:     sync.RWMutex{},
	}
}

func (sc *sendConn) timestamp() time.Time {
	sc.mu.RLock()
	t := sc.ts
	sc.mu.RUnlock()
	return t
}

func (sc *sendConn) setTimestamp(ts time.Time) {
	sc.mu.Lock()
	sc.ts = ts
	sc.mu.Unlock()
}

type UdpForwarder struct {
	sendConnMap map[string]*sendConn
	Port        int
	ErrCh       chan error
	Cconn       *net.UDPConn
	mu          sync.RWMutex
	Dst         *Dst
	stopTickCh  chan struct{}
}

func NewUdpForwarder() *UdpForwarder {
	u := &UdpForwarder{
		sendConnMap: make(map[string]*sendConn),
		ErrCh:       make(chan error),
		mu:          sync.RWMutex{},
		stopTickCh:  make(chan struct{}, 1),
	}
	go u.Clean(300, 60)
	return u
}

func (f *UdpForwarder) Listen() error {
	addr := &net.UDPAddr{Port: f.Port}
	l, err := net.ListenUDP("udp", addr)
	if err != nil {
		return err
	}
	f.Cconn = l
	return nil
}

func (f *UdpForwarder) Close() error {
	f.stopTickCh <- struct{}{}
	return f.Cconn.Close()
}

func (f *UdpForwarder) Accept() {
	buf := bytespool.Alloc(int32(largeBufferSize))
	defer bytespool.Free(buf)

	// receive from client
	n, srcAddr, err := f.Cconn.ReadFromUDP(buf)
	if err != nil {
		log.Println("conn accept error", err)
		return
	}

	var (
		sc *sendConn
	)
	f.mu.Lock()
	if v, ok := f.sendConnMap[srcAddr.String()]; !ok {
		dstAddr, err := net.ResolveUDPAddr("udp", f.Dst.Addr)
		sconn, err := net.DialUDP(f.Dst.Network, nil, dstAddr)
		if err != nil {
			log.Printf("dial target err:%v\n", err)
			f.ErrCh <- err
			return
		}

		sc = newSendConn(srcAddr, sconn)
		f.sendConnMap[sc.src.String()] = sc
		go sc.handleConn(f.Cconn)
	} else {
		sc = v
	}
	f.mu.Unlock()

	sc.conn.SetWriteDeadline(time.Now().Add(10 * time.Second))
	// send to server
	_, err = sc.conn.Write(buf[:n])
	if err != nil {
		log.Println("sconn write error", err)
	}
}

func (s *sendConn) handleConn(clientConn *net.UDPConn) {
	buf := bytespool.Alloc(int32(largeBufferSize))
	defer func() {
		bytespool.Free(buf)
		s.conn.Close()
		log.Printf("send conn %s closed\n", s.src.String())
	}()

	go func() {
		for {
			// receive from server
			n, _, err := s.conn.ReadFromUDP(buf)
			if err != nil {
				log.Println("sconn read error", err)
				return
			}
			s.setTimestamp(time.Now())
			s.readCh <- n
		}
	}()

	for {
		select {
		case <-s.stopCh:
			return
		case n := <-s.readCh:
			// send to client
			_, err := clientConn.WriteToUDP(buf[:n], s.src)
			if err != nil {
				log.Println("cconn write error", err)
				return
			}
		}
	}
}

func (f *UdpForwarder) Clean(threshold int64, interval int) {
	ticker := time.NewTicker(time.Duration(interval) * time.Second)
	defer ticker.Stop()

	var (
		deleteSendConns []string
		srcAddr         string
		sc              *sendConn
	)
	for {
		select {
		case <-f.stopTickCh:
			return
		case <-ticker.C:
			f.mu.Lock()
			log.Println("number of goroutine", runtime.NumGoroutine())
			log.Println("sendConnMap len", len(f.sendConnMap))
			for srcAddr, sendConn := range f.sendConnMap {
				if time.Now().Unix()-sendConn.timestamp().Unix() >= threshold {
					deleteSendConns = append(deleteSendConns, srcAddr)
				}
			}

			log.Println("deleteSendConns", len(deleteSendConns))
			for len(deleteSendConns) > 0 {
				srcAddr = deleteSendConns[0]
				sc = f.sendConnMap[srcAddr]
				sc.stopCh <- struct{}{}
				delete(f.sendConnMap, srcAddr)
				deleteSendConns = deleteSendConns[1:]
			}
			f.mu.Unlock()
		}
	}
}

func (f *UdpForwarder) SetPort(port int) {
	f.Port = port
}

func (f *UdpForwarder) SetDst(d *Dst) {
	f.Dst = d
}

func (f *UdpForwarder) ErrChan() chan error {
	return f.ErrCh
}

func (f *UdpForwarder) SendConnNum() int {
	f.mu.RLock()
	n := len(f.sendConnMap)
	f.mu.RUnlock()
	return n
}
