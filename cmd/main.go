package main

import (
	"flag"
	"log"
	"os"
	"os/signal"
	"syscall"

	"gitlab.com/RandomCivil/common"
	"gitlab.com/RandomCivil/common/profile"
	"gitlab.com/RandomCivil/forward"
)

var (
	port    = flag.Int("port", 2000, "Listening port")
	timeout = flag.Int("timeout", 5, "Dial destination timeout")
	dst     = flag.String("dst", "", "Destination Address")
	version = flag.Bool("version", false, "Show current version")
)

func main() {
	flag.Parse()
	common.Verbose()
	if *version {
		return
	}

	hup := make(chan os.Signal)
	signal.Notify(hup, syscall.SIGHUP)
	f, err := forward.NewForwarder(*port, *dst, *timeout)
	if err != nil {
		return
	}
	go f.Start()
	go func() {
		for {
			select {
			case <-hup:
				profile.PprofStat()
			}
		}
	}()
	select {
	case err := <-f.ErrCh:
		log.Printf("err:%+v\n", err)
	}
}
