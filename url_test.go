package forward

import (
	"reflect"
	"testing"
)

func TestParseDst(t *testing.T) {
	cases := []struct {
		dst      string
		expected []string
	}{
		{"tcp://10.0.0.1:3000", []string{"tcp", "10.0.0.1:3000"}},
		{"tcp6://[::]:3000", []string{"tcp6", "[::]:3000"}},
		{"udp://10.0.0.1:3000", []string{"udp", "10.0.0.1:3000"}},
	}
	for _, c := range cases {
		d := NewDst(c.dst)
		d.parse()
		out := []string{d.Network, d.Addr}
		if !reflect.DeepEqual(out, c.expected) {
			t.Fatalf("TestParseDst error,out:%v,expected:%v\n", out, c.expected)
		}
	}
}

func TestParseErrDst(t *testing.T) {
	dsts := []string{"abc://10.0.0.1:3000", "我://10.0.0.1:3000/"}
	for _, dst := range dsts {
		d := NewDst(dst)
		err := d.parse()
		if err == nil {
			t.Fatalf("TestParseErrDst error,dst:%s\n", dst)

		}
	}
}
