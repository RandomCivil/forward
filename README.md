TCP/UDP 转发器

## usage

```shell
  -dst string
    	Destination Address
  -port int
    	Listening port (default 2000)
  -timeout int
    	Dial destination timeout (default 5)
  -version
    	Show current version

forward -dst=udp://100.100.100.100:3000 -port=3000

forward -dst=tcp://100.100.100.100:3000 -port=3000
```
